document.addEventListener("DOMContentLoaded", function () {
  // new Module.Field('#first-container');
  new Module.Field_1('#second-container');
});

var Module = (function(){

  var Module = {};

  function DomQueryHandler(query, context, toCollection) {
    var searchContext,
        queryResult,
        queryType = typeof query,
        domQueryResult = [];
    toCollection = !!toCollection;

    if (typeof context !== 'undefined' && context instanceof DomQueryObject) {
      searchContext = context.getNodeElement();
    } else {
      searchContext = document;
    }

    switch (queryType) {
      case 'string':
        queryResult = searchContext.querySelectorAll(query);
        queryResult.forEach(function (item) {
          domQueryResult.push(new DomQueryObject(item))
        });

        if (!queryResult.length) {
          console.warn('Object by selector "' + query + '" not found!')
        }

        break;
      case 'object':
        domQueryResult.push(new DomQueryObject(query));
        break;
      default:
        throw new Error('Bad argument Exception. You can use arguments of string or object type only!!!');
    }

    if (toCollection || domQueryResult.length > 1) {
      return new DomQueryCollection(domQueryResult);
    } else {
      return domQueryResult[0];
    }
  }

  DomQueryHandler.createElement = function (tagName) {
    var element = document.createElement(tagName);
    return new DomQueryObject(element);
  };

  function DomQueryCollection(domQueryObjects) {

    this.length = function () {
      return domQueryObjects.length;
    };

    this.bind = function (eventType, eventListener) {
      domQueryObjects.forEach(function (item) {
        item.bind(eventType, eventListener)
      });
    };

    this.forEach = function (callback) {
      domQueryObjects.forEach(callback);
    };

    this.eq = function (index) {
      return domQueryObjects[index];
    };

    this.remove = function () {
      domQueryObjects.forEach(function (queryObject) {
        queryObject.remove();
      });
    };
  }

  function DomQueryObject(nodeElement) {

    this.data = function (key, value) {
      if (typeof value !== 'undefined') {
        nodeElement.dataset[key] = value;
        return value;
      }
      return nodeElement.dataset[key];
    };

    this.bind = function (eventType, eventListener) {
      nodeElement.addEventListener(eventType, eventListener);
    };

    this.css = function (property, value) {
      nodeElement.style[property] = value;
    };

    this.attr = function (attributeName, value) {
      if (typeof value !== 'undefined') {
        nodeElement.setAttribute(attributeName, value);
      }
      return nodeElement.getAttribute(attributeName);
    };

    this.addClass = function (className) {
      nodeElement.classList.add(className);
      return this;
    };

    this.addClasses = function (classList) {
      classList.forEach(function (classname) {
        nodeElement.classList.add(classname);
      });
      return this;
    };

    this.hasClass = function (className) {
      return nodeElement.classList.contains(className);
    };

    this.removeClass = function (className) {
      nodeElement.classList.remove(className);
      return this;
    };

    this.getNodeElement = function () {
      return nodeElement;
    };

    this.append = function (childElement) {
      nodeElement.appendChild(childElement.findNodeElement());
    };

    this.html = function (html) {
      if (typeof html !== 'undefined') {
        nodeElement.innerHTML = html;

        return html;
      }
      return nodeElement.innerHTML;
    };

    this.remove = function () {
      nodeElement.remove();
    };

    this.eq = function () {
      return this;
    };

    this.parent = function () {
      return new DomQueryObject(nodeElement.parentElement)
    };

    this.nodeProperty = function (key, value) {
      if (typeof value !== 'undefined') {
        nodeElement[key] = value;
        return value;
      }
      return nodeElement[key];
    };

    this.lastChild = function () {

      return new DomQueryObject(nodeElement.lastChild);
    };
  }

  Module.DomModule = DomQueryHandler;

  return Module;
})();

Module = (function (m) {
  var $ = m.DomModule;

  m.Field = Field;

  return m;

  function Field(containerSelector) {
    var __showClassName = 'visible',
        __rowCount = 4,
        __colCount = 4,
        __containerSelector = containerSelector,
        __rowRemoveButton,
        __colRemoveButton;

    if (!containerSelector) {
      throw new Error('Bad argument Exception. Argument "containerSelector" required!');
    }

    var __mainContainer = $(__containerSelector);

    buildField();

    function cellEventHandler(event) {
      moveRemoveButtons(event);
      showRemoveButtons();
    }

    function showRemoveButtons() {
      if (getRowCount() > 1) {
        __rowRemoveButton.addClass(__showClassName);
      } else {
        __rowRemoveButton.removeClass(__showClassName);
      }

      if (getColCount() > 1) {
        __colRemoveButton.addClass(__showClassName);
      } else {
        __colRemoveButton.removeClass(__showClassName);
      }
    }

    function moveRemoveButtons (event) {
      var spanTarget = $(event.target),
          offsetTop = spanTarget.nodeProperty('offsetTop'),
          offsetLeft = spanTarget.nodeProperty('offsetLeft');
      __rowRemoveButton.data('row_id', spanTarget.data('row_id'));
      __colRemoveButton.data('col_id', spanTarget.data('col_id'));
      __rowRemoveButton.css('top', offsetTop + 'px');
      __colRemoveButton.css('left', offsetLeft + 'px');
    }

    function hideRemoveButtons () {
      __colRemoveButton.removeClass(__showClassName);
      __rowRemoveButton.removeClass(__showClassName);
    }

    function addRow() {
      var lastRow = __mainContainer.lastChild().lastChild(),
          cellList = $('.square', lastRow, true),
          fieldContainer = $('.field', __mainContainer),
          newRowId = +lastRow.data('row_id') + 1,
          newRow = generateFieldRow(newRowId);
      fieldContainer.append(newRow);
      cellList.forEach(function (oldCell) {
        var newCell = generateFieldCell(oldCell.data('col_id'), newRowId);
        newRow.append(newCell);
      });
    }

    function addCol() {
      var rowList = $('.row', __mainContainer, true);
      rowList.forEach(function (row) {
        var lastCell = row.lastChild(),
            newCell = generateFieldCell(+lastCell.data('col_id') + 1, lastCell.data('row_id'));
        row.append(newCell);
      });
    }

    function removeRow(event) {
      if (getRowCount() > 1) {
        var removeRowButton = $(event.target),
            targetRow = $('.row[data-row_id="' + removeRowButton.data('row_id') + '"]');
        hideRemoveButtons();
        targetRow.remove();
      }
    }

    function removeCol(event) {
      if (getColCount() > 1) {
        var removeColButton = $(event.target),
            targetCellList = $('.row .square[data-col_id="' + removeColButton.data('col_id') + '"]');
        hideRemoveButtons();
        targetCellList.remove();
      }
    }

    function buildField() {
      var fieldContainer = generateFieldContainer();
      fieldContainer.bind('mouseleave', hideRemoveButtons);
      __mainContainer.append(fieldContainer);
      fillFieldContainer(fieldContainer, __rowCount, __colCount);
    }

    function generateFieldContainer() {
      var fieldContainer = $.createElement('div'),
          addRowButton = generateRowAddButton(),
          addColButton = generateColAddButton();
      __rowRemoveButton = generateRowRemoveButton();
      __colRemoveButton = generateColRemoveButton();

      fieldContainer.addClass('field');
      fieldContainer.append(__rowRemoveButton);
      fieldContainer.append(__colRemoveButton);
      fieldContainer.append(addRowButton);
      fieldContainer.append(addColButton);
      return fieldContainer;
    }

    function fillFieldContainer(fieldContainer, rowCount, colCount) {
      for (var i = 0; i < rowCount; i++) {
        var row = generateFieldRow(i, colCount);
        fieldContainer.append(row);
      }
    }

    function generateFieldRow(rowId, colCount) {
      var row = $.createElement('div');
      row.addClass('row');
      row.data('row_id', rowId);
      for (var j = 0; j < +colCount; j++) {
        var cell = generateFieldCell(j, rowId);
        row.append(cell);
      }
      return row;
    }

    function generateFieldCell(colId, rowId) {
      var cell = $.createElement('span');
      cell.bind('mouseenter', cellEventHandler);
      cell.addClass('square');
      cell.data('row_id', rowId);
      cell.data('col_id', colId);
      return cell;
    }

    function generateColRemoveButton() {
      var colRemoveButton = generateRemoveButton(['remove-col']);
      colRemoveButton.bind('click', removeCol);
      return colRemoveButton
    }

    function generateRowRemoveButton() {
      var rowRemoveButton = generateRemoveButton(['remove-row']);
      rowRemoveButton.bind('click', removeRow);
      return rowRemoveButton;
    }

    function generateColAddButton() {
      var colAddButton = generateAddButton(['add-col']);
      colAddButton.bind('click', addCol);
      return colAddButton;
    }

    function generateRowAddButton() {
      var rowAddButton = generateAddButton(['add-row']);
      rowAddButton.bind('click', addRow);
      return rowAddButton;
    }

    function generateRemoveButton(classList) {
      var removeButton = generateButton(classList.concat([
            'button-remove'
          ]
      ));
      removeButton.bind('mouseenter', showHoveredButton);
      return removeButton;
    }

    function generateAddButton(classList) {
      return generateButton(['button-add'].concat(classList));
    }

    function generateButton(classList) {
      var button = $.createElement('span');
      button.addClasses([
        'square',
        'button'
      ].concat(classList));
      /*button.bind('mouseenter', function (event) {
        event.stopPropagation();
        hideRemoveButtons();
      });*/
      return button;
    }

    function showHoveredButton(event) {
      var removeButton = $(event.target);
      removeButton.addClass(__showClassName);
    }

    function getColCount() {
      return $('.square', __mainContainer.lastChild().lastChild(), true).length();
    }

    function getRowCount() {
      return $('.row', __mainContainer, true).length();
    }

  }
})(Module || {});

Module = (function (m) {
  var $ = m.DomModule;

  m.Field_1 = Field;

  return m;

  function Field(containerSelector) {
    var __showClassName = 'visible',
        __oneRowClass = 'height-of-1',
        __oneColClass = 'width-of-1',
        __rowCount = 4,
        __colCount = 4,
        __containerSelector = containerSelector,
        __removeTarget = {
          rowObject: null,
          collCollection: null
        },
        __rowRemoveButton,
        __colRemoveButton;

    if (!containerSelector) {
      throw new Error('Bad argument Exception. Argument "containerSelector" required!');
    }

    var __mainContainer = $(__containerSelector);

    buildField();

    /*function cellEventHandler(event) {
      moveRemoveButtons(event);
      showRemoveButtons();
    }*/

    /*function showRemoveButtons() {
        __rowRemoveButton.addClass(__showClassName);
        __colRemoveButton.addClass(__showClassName);
    }*/

    function moveRemoveButtons(event) {
      var spanTarget = $(event.target),
          offsetTop = spanTarget.nodeProperty('offsetTop'),
          offsetLeft = spanTarget.nodeProperty('offsetLeft');
      catchRemoveTargets(spanTarget);
      __rowRemoveButton.css('top', offsetTop + 'px');
      __colRemoveButton.css('left', offsetLeft + 'px');
      __rowRemoveButton.addClass(__showClassName);
      __colRemoveButton.addClass(__showClassName);
    }

    function catchRemoveTargets(spanTarget) {
      __removeTarget.rowObject = spanTarget.parent();
      $('.square', spanTarget.parent(), true).forEach(function (cell, index) {
        if (spanTarget.findNodeElement() === cell.findNodeElement()) {
          __removeTarget.collCollection = $('.row .square:nth-child(' + (index + 1) + ')', __mainContainer);
        }
      });

    }

    function hideRemoveButtons() {
      // setTimeout(function () {
        __colRemoveButton.removeClass(__showClassName);
        __rowRemoveButton.removeClass(__showClassName);
      // }, 5000);
    }

    function addRow() {
      __mainContainer.lastChild().append(generateFieldRow(getColCount()));
      if (getRowCount() > 1) {
        __mainContainer.lastChild().removeClass(__oneRowClass);
      }
    }

    function addCol() {
      var rowList = $('.row', __mainContainer, true);
      rowList.forEach(function (row) {
        row.append(generateFieldCell());
      });
      if (getColCount() > 1) {
        __mainContainer.lastChild().removeClass(__oneColClass);
      }
    }

    function removeRow() {
        hideRemoveButtons();
      __rowRemoveButton.css('top', 0);
      __removeTarget.rowObject.remove();
      if (getRowCount() <= 1) {
        __mainContainer.lastChild().addClass(__oneRowClass);
      }
    }

    function removeCol() {
        hideRemoveButtons();
      __colRemoveButton.css('left', 0);
      __removeTarget.collCollection.remove();
      if (getColCount() <= 1) {
        __mainContainer.lastChild().addClass(__oneColClass);
      }
    }

    function buildField() {
      var fieldContainer = generateFieldContainer();
      // fieldContainer.bind('mouseenter', showRemoveButtons);
      fieldContainer.bind('mouseleave', hideRemoveButtons);
      __mainContainer.append(fieldContainer);
      fillFieldContainer(fieldContainer, __rowCount, __colCount);
    }

    function generateFieldContainer() {
      var fieldContainer = $.createElement('div'),
          addRowButton = generateRowAddButton(),
          addColButton = generateColAddButton();
      __rowRemoveButton = generateRowRemoveButton();
      __colRemoveButton = generateColRemoveButton();

      fieldContainer.addClass('field');
      fieldContainer.append(__rowRemoveButton);
      fieldContainer.append(__colRemoveButton);
      fieldContainer.append(addRowButton);
      fieldContainer.append(addColButton);
      return fieldContainer;
    }

    function fillFieldContainer(fieldContainer, rowCount, colCount) {
      for (var i = 0; i < rowCount; i++) {
        var row = generateFieldRow(colCount);
        fieldContainer.append(row);
      }
    }

    function generateFieldRow(colCount) {
      var row = $.createElement('div');
      row.addClass('row');
      for (var j = 0; j < colCount; j++) {
        row.append(generateFieldCell());
      }
      return row;
    }

    function generateFieldCell() {
      var cell = $.createElement('span');
      // cell.bind('mouseenter', cellEventHandler);
      cell.bind('mouseenter', moveRemoveButtons);
      cell.addClass('square');
      return cell;
    }

    function generateColRemoveButton() {
      var colRemoveButton = generateRemoveButton(['remove-col']);
      colRemoveButton.bind('click', removeCol);
      return colRemoveButton
    }

    function generateRowRemoveButton() {
      var rowRemoveButton = generateRemoveButton(['remove-row']);
      rowRemoveButton.bind('click', removeRow);
      return rowRemoveButton;
    }

    function generateColAddButton() {
      var colAddButton = generateAddButton(['add-col']);
      colAddButton.bind('click', addCol);
      return colAddButton;
    }

    function generateRowAddButton() {
      var rowAddButton = generateAddButton(['add-row']);
      rowAddButton.bind('click', addRow);
      return rowAddButton;
    }

    function generateRemoveButton(classList) {
      var removeButton = generateButton(classList.concat([
            'button-remove'
          ]
      ));
      removeButton.bind('mouseenter', showHoveredButton);
      return removeButton;
    }

    function generateAddButton(classList) {
      return generateButton(['button-add'].concat(classList));
    }

    function generateButton(classList) {
      var button = $.createElement('span');
      button.addClasses([
        'square',
        'button'
      ].concat(classList));
      return button;
    }

    function showHoveredButton(event) {
      var hoveredRemoveButton = $(event.target);

      if (hoveredRemoveButton.findNodeElement() === __colRemoveButton.findNodeElement()) {
        __rowRemoveButton.removeClass(__showClassName);
      } else {
        __colRemoveButton.removeClass(__showClassName);
      }
    }

    function getColCount() {
      return $('.square', __mainContainer.lastChild().lastChild(), true).length();
    }

    function getRowCount() {
      return $('.row', __mainContainer, true).length();
    }
  }
})(Module || {});
