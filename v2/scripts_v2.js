/**
 * Field
 */
class Field {
  /**
   * Field constructor
   *
   * @param {HTMLElement} mainContainer
   */
  constructor(mainContainer) {
    this._hideClassName = `field__button_hidden`;
    this._mainContainer = mainContainer;
    this._rowRemoveButton = this._mainContainer
        .querySelector(`.field__button_remove-row`);
    this._colRemoveButton = this._mainContainer
        .querySelector(`.field__button_remove-col`);
    this._cellTarget = null;
    this._hideButtonsTimerId = 0;
    this.hideTimer = 2000;

    this._bindMethodsInstanceContext();
    this._bindEventHandlers();
  }

  // _someMethod = () => {};

  /**
   *
   * @private
   */
  _bindMethodsInstanceContext() {
    [
      `_fieldMouseEnterHandler`,
      `_fieldMouseLeaveHandler`,
      `_hideRemoveButtons`,
      `_fieldMouseOverHandler`,
      `_removeRow`,
      `_removeCol`,
      `_showHoveredRemoveButton`,
      `_removeButtonMouseLeaveHandler`,
      `_addRow`,
      `_addCol`,
    ].forEach((methodName) => this[methodName] = this[methodName].bind(this));
  }

  /**
   *
   * @private
   */
  _bindEventHandlers() {
    const fieldTable = this._mainContainer.querySelector(`.field__table`);
    fieldTable.addEventListener(`mouseenter`, this._fieldMouseEnterHandler);
    fieldTable.addEventListener(`mouseleave`, this._fieldMouseLeaveHandler);
    fieldTable.addEventListener(`mouseover`, this._fieldMouseOverHandler);
    this._rowRemoveButton.addEventListener(`click`, this._removeRow);
    this._colRemoveButton.addEventListener(`click`, this._removeCol);
    [this._rowRemoveButton, this._colRemoveButton].forEach((removeButton) =>
        removeButton.addEventListener(
            `mouseenter`, this._showHoveredRemoveButton
        )
    );
    [this._rowRemoveButton, this._colRemoveButton].forEach((removeButton) =>
        removeButton.addEventListener(
            `mouseleave`,
            this._removeButtonMouseLeaveHandler
        )
    );
    this._mainContainer.querySelector(`.field__button_add-row`)
        .addEventListener(
            `click`,
            this._addRow
        );
    this._mainContainer.querySelector(`.field__button_add-col`)
        .addEventListener(
            `click`,
            this._addCol
        );
  }


  /**
   * Show remove buttons when cursor enter in "field"
   *
   * @private
   * @return {void}
   */
  _showRemoveButtons() {
    if (this._getRowCount() > 1) {
      this._rowRemoveButton.classList.remove(this._hideClassName);
    }

    if (this._getColCount() > 1) {
      this._colRemoveButton.classList.remove(this._hideClassName);
    }
  }

  /**
   * Hide remove buttons when cursor leave "field"
   *
   * @private
   * @return {void}
   */
  _hideRemoveButtons() {
    this._rowRemoveButton.classList.add(this._hideClassName);
    this._colRemoveButton.classList.add(this._hideClassName);
  }

  /**
   * Remove row
   *
   * @private
   * @return {void}
   */
  _removeRow() {
    if (this._getRowCount() > 1) {
      this._cellTarget.parentElement.remove();
      this._hideRemoveButtons();
    }
  }

  /**
   * Remove col
   *
   * @private
   * @return {void}
   */
  _removeCol() {
    if (this._getColCount() > 1) {
      const cellList = this._cellTarget.parentElement.children;
      const collIndex = [].indexOf.call(cellList, this._cellTarget);
      this._getRowList().forEach((row) => row.children[collIndex].remove());
      this._hideRemoveButtons();
    }
  }

  /**
   * Add row
   *
   * @private
   * @return {void}
   */
  _addRow() {
    const newRow = document.createElement(`tr`);
    newRow.className = `field__row`;
    for (let i = 0; i < this._getColCount(); i++) {
      const newCell = this._generateCell();
      newRow.appendChild(newCell);
    }
    this._mainContainer.querySelector(`.field__table>tbody`)
        .appendChild(newRow);
  }

  /**
   * Add col
   *
   * @private
   * @return {void}
   */
  _addCol() {
    const rowList = this._mainContainer.querySelectorAll(`.field__row`);
    rowList.forEach((row) => row.appendChild(this._generateCell()));
  }

  /**
   * Generate cell
   *
   * @private
   * @return {HTMLTableCellElement}
   */
  _generateCell() {
    const newCell = document.createElement(`td`);
    newCell.className = `field__cell table__cell`;
    return newCell;
  }

  /**
   * Set cell target for remove in future
   *
   * @param {HTMLTableCellElement} targetCell
   * @private
   * @return {void}
   */
  _catchCellRemoveTarget(targetCell) {
    this._cellTarget = targetCell;
  }

  /**
   * Return count of rows
   *
   * @private
   * @return {number}
   */
  _getRowCount() {
    return this._getRowList().length;
  }

  /**
   * Return row list
   *
   * @return {NodeList}
   * @private
   */
  _getRowList() {
    return this._mainContainer.querySelectorAll(`.field__row`);
  }

  /**
   * Return count of col
   *
   * @private
   * @return {number}
   */
  _getColCount() {
    return this._mainContainer
        .querySelectorAll(`.field__row:nth-child(1)>.field__cell`).length;
  }

  /**
   * Show one remove button (hovered)
   *
   * @param {MouseEvent} event
   * @private
   * @return {void}
   */
  _showHoveredRemoveButton(event) {
    const hoveredRemoveButton = event.target;
    clearTimeout(this._hideButtonsTimerId);
    if (Object.is(hoveredRemoveButton, this._rowRemoveButton)) {
      this._colRemoveButton.classList.add(this._hideClassName);
    } else {
      this._rowRemoveButton.classList.add(this._hideClassName);
    }
  }

  /**
   * Hide hovered remove button
   *
   * @private
   * @return {void}
   */
  _removeButtonMouseLeaveHandler() {
    this._hideButtonsTimerId = setTimeout(
        this._hideRemoveButtons,
        this.hideTimer
    );
  }

  /**
   * Field event handler (mouseOver)
   *
   * @param {MouseEvent} event
   * @private
   * @return {void}
   */
  _fieldMouseOverHandler(event) {
    if (event.target instanceof HTMLTableCellElement) {
      const cell = event.target;
      this._moveColRemoveButton(cell);
      this._moveRowRemoveButton(cell);
      this._catchCellRemoveTarget(cell);
    }
  }

  /**
   * Move col remove button on current position by cursor
   *
   * @param {HTMLTableCellElement} cell
   * @private
   * @return {void}
   */
  _moveColRemoveButton(cell) {
    this._colRemoveButton.style.left =
        `${cell.offsetLeft - cell.parentElement.offsetLeft}px`;
  }

  /**
   * Move row remove button on current position by cursor
   *
   * @param {HTMLTableCellElement} cell
   * @private
   * @return {void}
   */
  _moveRowRemoveButton(cell) {
    this._rowRemoveButton.style.top = `${cell.offsetTop}px`;
  }

  /**
   * Field handler (mouseEnter)
   *
   * @private
   * @return {void}
   */
  _fieldMouseEnterHandler() {
    this._showRemoveButtons();
    clearTimeout(this._hideButtonsTimerId);
  }

  /**
   * Field handler (mouseLeave)
   *
   * @private
   * @return {void}
   */
  _fieldMouseLeaveHandler() {
    this._hideButtonsTimerId = setTimeout(
        this._hideRemoveButtons,
        this.hideTimer
    );
  }
}

new Field(document.getElementById(`container_1`));
