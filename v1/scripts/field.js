Module = (function (m) {
  var $ = m.DomModule;

  m.Field = Field;

  return m;

  function Field(containerSelector) {
    var __showClassName = 'visible',
        __rowCount = 4,
        __colCount = 4,
        __containerSelector = containerSelector,
        __rowRemoveButton,
        __colRemoveButton;

    if (!containerSelector) {
      throw new Error('Bad argument Exception. Argument "containerSelector" required!');
    }

    var __mainContainer = $(__containerSelector);

    buildField();

    function rowEventHandler(event) {
      moveRowRemoveButton(event);
      showRemoveButtons();
    }

    function showRemoveButtons() {
      if (getRowCount() > 1) {
        __rowRemoveButton.addClass(__showClassName);
      } else {
        __rowRemoveButton.removeClass(__showClassName);
      }

      if (getColCount() > 1) {
        __colRemoveButton.addClass(__showClassName);
      } else {
        __colRemoveButton.removeClass(__showClassName);
      }
    }

    function moveColRemoveButton (event) {
      var spanTarget = $(event.target),
          offsetLeft = spanTarget.nodeProperty('offsetLeft');
      __colRemoveButton.data('col_id', spanTarget.data('col_id'));
      __colRemoveButton.css('left', offsetLeft + 'px');
    }

    function moveRowRemoveButton(event) {
      var rowTarget = $(event.target),
          offsetTop = rowTarget.lastChild().nodeProperty('offsetTop');
      __rowRemoveButton.data('row_id', rowTarget.data('row_id'));
      __rowRemoveButton.css('top', offsetTop + 'px');
    }

    function hideRemoveButtons () {
      __colRemoveButton.removeClass(__showClassName);
      __rowRemoveButton.removeClass(__showClassName);
    }

    function addRow() {
      var lastRow = __mainContainer.lastChild().lastChild(),
          cellList = $('.square', lastRow, true),
          fieldContainer = $('.field', __mainContainer),
          newRowId = +lastRow.data('row_id') + 1,
          newRow = generateFieldRow(newRowId);
      fieldContainer.append(newRow);
      cellList.forEach(function (oldCell) {
        var newCell = generateFieldCell(oldCell.data('col_id'), newRowId);
        newRow.append(newCell);
      });
    }

    function addCol() {
      var rowList = $('.row', __mainContainer, true);
      rowList.forEach(function (row) {
        var lastCell = row.lastChild(),
            newCell = generateFieldCell(+lastCell.data('col_id') + 1, lastCell.data('row_id'));
        row.append(newCell);
      });
    }

    function removeRow(event) {
      if (getRowCount() > 1) {
        var removeRowButton = $(event.target),
            targetRow = $('.row[data-row_id="' + removeRowButton.data('row_id') + '"]', __mainContainer);
        hideRemoveButtons();
        targetRow.remove();
      }
    }

    function removeCol(event) {
      if (getColCount() > 1) {
        var removeColButton = $(event.target),
            targetCellList = $('.row .square[data-col_id="' + removeColButton.data('col_id') + '"]', __mainContainer);
        hideRemoveButtons();
        targetCellList.remove();
      }
    }

    function showHoveredButton(event) {
      var removeButton = $(event.target);
      removeButton.addClass(__showClassName);
    }

    function getColCount() {
      return $('.square', __mainContainer.lastChild().lastChild(), true).length();
    }

    function getRowCount() {
      return $('.row', __mainContainer, true).length();
    }

    function buildField() {
      var fieldContainer = generateFieldContainer();
      fieldContainer.bind('mouseleave', hideRemoveButtons);
      __mainContainer.append(fieldContainer);
      fillFieldContainer(fieldContainer, __rowCount, __colCount);
    }

    function generateFieldContainer() {
      var fieldContainer = $.createElement('div'),
          addRowButton = generateRowAddButton(),
          addColButton = generateColAddButton();
      __rowRemoveButton = generateRowRemoveButton();
      __colRemoveButton = generateColRemoveButton();

      fieldContainer.addClass('field');
      fieldContainer.append(__rowRemoveButton);
      fieldContainer.append(__colRemoveButton);
      fieldContainer.append(addRowButton);
      fieldContainer.append(addColButton);
      return fieldContainer;
    }

    function fillFieldContainer(fieldContainer, rowCount, colCount) {
      for (var i = 0; i < rowCount; i++) {
        var row = generateFieldRow(i, colCount);
        fieldContainer.append(row);
      }
    }

    function generateFieldRow(rowId, colCount) {
      var row = $.createElement('div');
      row.addClass('row');
      row.data('row_id', rowId);
      row.bind('mouseenter', rowEventHandler);
      for (var j = 0; j < +colCount; j++) {
        var cell = generateFieldCell(j, rowId);
        row.append(cell);
      }
      return row;
    }

    function generateFieldCell(colId, rowId) {
      var cell = $.createElement('span');
      cell.bind('mouseenter', moveColRemoveButton);
      cell.addClass('square');
      cell.data('row_id', rowId);
      cell.data('col_id', colId);
      return cell;
    }

    function generateColRemoveButton() {
      var colRemoveButton = generateRemoveButton(['remove-col']);
      colRemoveButton.bind('click', removeCol);
      return colRemoveButton
    }

    function generateRowRemoveButton() {
      var rowRemoveButton = generateRemoveButton(['remove-row']);
      rowRemoveButton.bind('click', removeRow);
      return rowRemoveButton;
    }

    function generateColAddButton() {
      var colAddButton = generateAddButton(['add-col']);
      colAddButton.bind('click', addCol);
      return colAddButton;
    }

    function generateRowAddButton() {
      var rowAddButton = generateAddButton(['add-row']);
      rowAddButton.bind('click', addRow);
      return rowAddButton;
    }

    function generateRemoveButton(classList) {
      var removeButton = generateButton(classList.concat([
            'button-remove'
          ]
      ));
      removeButton.bind('mouseenter', showHoveredButton);
      return removeButton;
    }

    function generateAddButton(classList) {
      return generateButton(['button-add'].concat(classList));
    }

    function generateButton(classList) {
      var button = $.createElement('span');
      button.addClasses([
        'square',
        'button'
      ].concat(classList));
      return button;
    }
  }
})(Module || {});
