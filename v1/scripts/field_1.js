Module = (function(m) {
  var $ = m.DomModule;

  m.Field_1 = Field;

  return m;

  function Field(containerSelector) {
    var __showClassName = 'visible',
      __rowCount = 4,
      __colCount = 4,
      __containerSelector = containerSelector,
      __removeTarget = {
        rowObject: null,
        collCollection: null,
      },
      __rowRemoveButton,
      __colRemoveButton;

    if (!containerSelector) {
      throw new Error(
        'Bad argument Exception. Argument "containerSelector" required!'
      );
    }

    var __mainContainer = $(__containerSelector);

    buildField();

    function rowEventHandler(event) {
      moveRowRemoveButton(event);
      showRemoveButtons();
    }

    function showRemoveButtons() {
      if (getRowCount() > 1) {
        __rowRemoveButton.addClass(__showClassName);
      } else {
        __rowRemoveButton.removeClass(__showClassName);
      }

      if (getColCount() > 1) {
        __colRemoveButton.addClass(__showClassName);
      } else {
        __colRemoveButton.removeClass(__showClassName);
      }
    }

    function moveColRemoveButton(event) {
      var spanTarget = $(event.target),
        offsetLeft = spanTarget.nodeProperty('offsetLeft');
      catchRemoveTargets(spanTarget);
      __colRemoveButton.css('left', offsetLeft + 'px');
    }

    function moveRowRemoveButton(event) {
      var rowTarget = $(event.target),
        offsetTop = rowTarget.lastChild().nodeProperty('offsetTop');
      __rowRemoveButton.css('top', offsetTop + 'px');
    }

    function catchRemoveTargets(spanTarget) {
      __removeTarget.rowObject = spanTarget.parent();
      $('.square', spanTarget.parent(), true).forEach(function(cell, index) {
        if (spanTarget.findNodeElement() === cell.findNodeElement()) {
          __removeTarget.collCollection = $(
            '.row .square:nth-child(' + (index + 1) + ')',
            __mainContainer
          );
        }
      });
    }

    function hideRemoveButtons() {
      __colRemoveButton.removeClass(__showClassName);
      __rowRemoveButton.removeClass(__showClassName);
    }

    function addRow() {
      __mainContainer.lastChild().append(generateFieldRow(getColCount()));
    }

    function addCol() {
      var rowList = $('.row', __mainContainer, true);
      rowList.forEach(function(row) {
        row.append(generateFieldCell());
      });
    }

    function removeRow() {
      if (getRowCount() > 1) {
        hideRemoveButtons();
        __removeTarget.rowObject.remove();
      }
    }

    function removeCol() {
      if (getColCount() > 1) {
        hideRemoveButtons();
        __removeTarget.collCollection.remove();
      }
    }

    function showHoveredButton(event) {
      var removeButton = $(event.target);
      removeButton.addClass(__showClassName);
    }

    function getColCount() {
      return $(
        '.square',
        __mainContainer.lastChild().lastChild(),
        true
      ).length();
    }

    function getRowCount() {
      return $('.row', __mainContainer, true).length();
    }

    function buildField() {
      var fieldContainer = generateFieldContainer();
      fieldContainer.bind('mouseleave', hideRemoveButtons);
      __mainContainer.append(fieldContainer);
      fillFieldContainer(fieldContainer, __rowCount, __colCount);
    }

    function generateFieldContainer() {
      var fieldContainer = $.createElement('div'),
        addRowButton = generateRowAddButton(),
        addColButton = generateColAddButton();
      __rowRemoveButton = generateRowRemoveButton();
      __colRemoveButton = generateColRemoveButton();

      fieldContainer.addClass('field');
      fieldContainer.append(__rowRemoveButton);
      fieldContainer.append(__colRemoveButton);
      fieldContainer.append(addRowButton);
      fieldContainer.append(addColButton);
      return fieldContainer;
    }

    function fillFieldContainer(fieldContainer, rowCount, colCount) {
      for (var i = 0; i < rowCount; i++) {
        var row = generateFieldRow(colCount);
        fieldContainer.append(row);
      }
    }

    function generateFieldRow(colCount) {
      var row = $.createElement('div');
      row.addClass('row');
      row.bind('mouseenter', rowEventHandler);
      for (var j = 0; j < colCount; j++) {
        row.append(generateFieldCell());
      }
      return row;
    }

    function generateFieldCell() {
      var cell = $.createElement('span');
      cell.bind('mouseenter', moveColRemoveButton);
      cell.addClass('square');
      return cell;
    }

    function generateColRemoveButton() {
      var colRemoveButton = generateRemoveButton(['remove-col']);
      colRemoveButton.bind('click', removeCol);
      return colRemoveButton;
    }

    function generateRowRemoveButton() {
      var rowRemoveButton = generateRemoveButton(['remove-row']);
      rowRemoveButton.bind('click', removeRow);
      return rowRemoveButton;
    }

    function generateColAddButton() {
      var colAddButton = generateAddButton(['add-col']);
      colAddButton.bind('click', addCol);
      return colAddButton;
    }

    function generateRowAddButton() {
      var rowAddButton = generateAddButton(['add-row']);
      rowAddButton.bind('click', addRow);
      return rowAddButton;
    }

    function generateRemoveButton(classList) {
      var removeButton = generateButton(classList.concat(['button-remove']));
      removeButton.bind('mouseenter', showHoveredButton);
      return removeButton;
    }

    function generateAddButton(classList) {
      return generateButton(['button-add'].concat(classList));
    }

    function generateButton(classList) {
      var button = $.createElement('span');
      button.addClasses(['square', 'button'].concat(classList));
      return button;
    }
  }
})(Module || {});
