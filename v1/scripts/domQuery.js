var Module = (function(){

  var Module = {};

  function DomQueryHandler(query, context, toCollection) {
    var searchContext,
        queryResult,
        queryType = typeof query,
        domQueryResult = [];
    toCollection = !!toCollection;

    if (typeof context !== 'undefined' && context instanceof DomQueryObject) {
      searchContext = context.getNodeElement();
    } else {
      searchContext = document;
    }

    switch (queryType) {
      case 'string':
        queryResult = searchContext.querySelectorAll(query);
        queryResult.forEach(function (item) {
          domQueryResult.push(new DomQueryObject(item))
        });

        if (!queryResult.length) {
          console.warn('Object by selector "' + query + '" not found!')
        }

        break;
      case 'object':
        domQueryResult.push(new DomQueryObject(query));
        break;
      default:
        throw new Error('Bad argument Exception. You can use arguments of string or object type only!!!');
    }

    if (toCollection || domQueryResult.length > 1) {
      return new DomQueryCollection(domQueryResult);
    } else {
      return domQueryResult[0];
    }
  }

  DomQueryHandler.createElement = function (tagName) {
    var element = document.createElement(tagName);
    return new DomQueryObject(element);
  };

  function DomQueryCollection(domQueryObjects) {

    this.length = function () {
      return domQueryObjects.length;
    };

    this.bind = function (eventType, eventListener) {
      domQueryObjects.forEach(function (item) {
        item.bind(eventType, eventListener)
      });
    };

    this.forEach = function (callback) {
      domQueryObjects.forEach(callback);
    };

    this.eq = function (index) {
      return domQueryObjects[index];
    };

    this.remove = function () {
      domQueryObjects.forEach(function (queryObject) {
        queryObject.remove();
      });
    };
  }

  function DomQueryObject(nodeElement) {

    this.data = function (key, value) {
      if (typeof value !== 'undefined') {
        nodeElement.dataset[key] = value;
        return value;
      }
      return nodeElement.dataset[key];
    };

    this.bind = function (eventType, eventListener) {
      nodeElement.addEventListener(eventType, eventListener);
    };

    this.css = function (property, value) {
      nodeElement.style[property] = value;
    };

    this.attr = function (attributeName, value) {
      if (typeof value !== 'undefined') {
        nodeElement.setAttribute(attributeName, value);
      }
      return nodeElement.getAttribute(attributeName);
    };

    this.addClass = function (className) {
      nodeElement.classList.add(className);
      return this;
    };

    this.addClasses = function (classList) {
      classList.forEach(function (classname) {
        nodeElement.classList.add(classname);
      });
      return this;
    };

    this.hasClass = function (className) {
      return nodeElement.classList.contains(className);
    };

    this.removeClass = function (className) {
      nodeElement.classList.remove(className);
      return this;
    };

    this.getNodeElement = function () {
      return nodeElement;
    };

    this.append = function (childElement) {
      nodeElement.appendChild(childElement.findNodeElement());
    };

    this.html = function (html) {
      if (typeof html !== 'undefined') {
        nodeElement.innerHTML = html;
        return html;
      }
      return nodeElement.innerHTML;
    };

    this.remove = function () {
      nodeElement.remove();
    };

    this.eq = function () {
      return this;
    };

    this.parent = function () {
      return new DomQueryObject(nodeElement.parentElement)
    };

    this.nodeProperty = function (key, value) {
      if (typeof value !== 'undefined') {
        nodeElement[key] = value;
        return value;
      }
      return nodeElement[key];
    };

    this.lastChild = function () {

      return new DomQueryObject(nodeElement.lastChild);
    };
  }

  Module.DomModule = DomQueryHandler;

  return Module;
})();
